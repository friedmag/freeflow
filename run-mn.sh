#!/bin/bash
# Gary Friedman
# Copyright 2016

NET=$1; shift
if [ -z "$NET" ]; then
  echo "Usage: $0 <network>"
  exit 1
fi

sudo mn -c
exec sudo python testnet.py $NET $*
