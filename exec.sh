#!/bin/bash
# Gary Friedman
# Copyright 2016

tgt=$1; shift
if [ -z "$tgt" ]; then echo "Missing target"; exit 1; fi

if [ "$tgt" = "random" ]; then
  hosts=$(ps -ef | grep bash | grep mininet:h | sed 's,^.*:,,')
  count=$(echo $hosts | wc -w)
  tgt=$(echo $hosts | cut -d" " -f $[RANDOM % count + 1])
  echo "========> Target: $tgt <========"
fi

pid=$(ps -ef | grep "mininet:$tgt$" | grep bash | awk '{print $2}')
if [ -z "$pid" ]; then echo "Unable to find target: $tgt"; exit 2; fi
exec sudo -E nsenter -t $pid -n $*
