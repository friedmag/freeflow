#!/bin/bash
# Gary Friedman
# Copyright 2016

_usage() {
  echo "Usage: $0 -h <host> [options]"
  echo -e "\t-h host\t\tMininet host to launch on (hostname or \"random\")"
  echo -e "\t-m mode\t\tMode to run in (freeflow or IP address of server for \"classic\") [$MODE]"
  echo -e "\t-n name\t\tService name to use (if classic mode, port#) [$NAME]"
  echo -e "\t-b bandwidth\tBandwidth to register with [$BW]"
  echo -e "\t-o op\t\tOperation to run (read, write, quick, sleep) [$OP]"
  echo -e "\t-c count\tNumber of times to run op [$COUNT]"
  exit 1
}

_debug() {
  echo "=====> $*"
}

MODE=freeflow
NAME=default
OP=quick
BW=1
COUNT=100
while getopts ":b:m:n:h:o:c:" opt; do
  case $opt in
    b) BW=$OPTARG; _debug "Bandwidth: $BW" ;;
    h) HOST=$OPTARG; _debug "Host: $HOST" ;;
    m) MODE=$OPTARG; _debug "Mode: $MODE" ;;
    n) NAME=$OPTARG; _debug "Name: $NAME" ;;
    c) COUNT=$OPTARG; _debug "Count: $COUNT" ;;
    o) OP=$OPTARG; _debug "Operation: $OP" ;;
    \?) _usage ;;
    :) echo "-$OPTARG requires an argument"; exit 1 ;;
  esac
done

[[ -z "$HOST" ]] && _usage

cd $(dirname $0)
exec ./exec.sh $HOST java/web-client/run.sh $MODE $NAME $OP $COUNT $BW
