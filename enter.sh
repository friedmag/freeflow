#!/bin/bash -x
# Gary Friedman
# Copyright 2016

tgt=$1; shift
if [ -z "$tgt" ]; then echo "Missing target"; exit 1; fi
exec $(dirname $0)/exec.sh $tgt bash -i
