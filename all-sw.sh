#!/bin/bash
# Gary Friedman
# Copyright 2016
# Example: sudo ./all-sw.sh "echo \$sw; ovs-ofctl dump-flows \$sw"

for sw in $(ovs-vsctl show | grep Bridge | awk '{print $2}' | xargs echo); do eval $*; done
