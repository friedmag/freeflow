// Gary Friedman
// Copyright 2016
'use strict';

const React = require('react');
const ReactDOM = require('react-dom');
const ForceChart = require('force-chart');

var CommentBox = React.createFactory(React.createClass({
  render: function() {
    return React.DOM.div({className: "netsvg"},
        ForceChart({dataUrl: '/network.json', updateRate: 5000}));
  },
}));

window.onload = () => {
  ReactDOM.render(
      CommentBox({}),
      document.getElementById('app')
  );
};
