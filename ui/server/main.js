// Gary Friedman
// Copyright 2016
'use strict';

const debug = require('debug')('freeflow:server');
const express = require('express');
const expressLess = require('express-less');
const browserify = require('browserify-middleware');
const app = express();

app.use('/js', browserify('ui/client'));
app.use('/less-css', expressLess('ui/client/less'));
app.use(express.static('ui/public'));
app.set('views', 'ui/views');
app.set('view engine', 'pug');

var ServiceMan, RouteMan;

app.get('/', (req, res) => {
  res.render('index');
});

app.get('/network.json', (req, res) => {
  //debug(`Retrieving network picture from RouteMan: ${RouteMan}`);
  // Ensure there are always at least empty lists for nodes/links
  var obj = Object.assign(RouteMan.network(), ServiceMan.services());
  var net = JSON.stringify(obj);
  //debug(`Responding with (nodes: ${obj.nodes.length}, links: ${obj.links.length}): ${net}`);
  res.end(net);
});

var server = app.listen(3000, () => {
  debug('FreeFlow console listening on port 3000!');
});

var io = require('socket.io')(server);

io.on('connection', (socket) => {
  socket.emit('update', RouteMan.network());
});

module.exports = (ServiceManIn, RouteManIn) => {
  ServiceMan = ServiceManIn;
  RouteMan = RouteManIn;
  RouteMan.on('update', (data) => {
    io.emit('update', data);
  });
  debug(`Configured`);
};
