 #!/usr/bin/python

import sys
from mininet.topo import Topo
from mininet.topolib import TorusTopo
from mininet.node import RemoteController
from mininet.net import Mininet
from mininet.util import dumpNodeConnections
from mininet.cli import CLI
from mininet.log import setLogLevel
from mininet.link import TCLink
from functools import partial

class Topo1(Topo):
    def build(self):
        s1 = self.addSwitch('s1')
        s2 = self.addSwitch('s2')
        s3 = self.addSwitch('s3')
        s4 = self.addSwitch('s4')
        h1 = self.addHost('h1')
        h2 = self.addHost('h2')
        h3 = self.addHost('h3')
        self.addLink(h1, s1)
        self.addLink(h2, s1)
        self.addLink(h3, s4)
        self.addLink(s2, s1)
        self.addLink(s3, s2)
        self.addLink(s4, s3)

class Multi1(Topo):
    def build(self):
        s1 = self.addSwitch('s1')
        s2 = self.addSwitch('s2')
        s3 = self.addSwitch('s3')
        s4 = self.addSwitch('s4')
        h1 = self.addHost('h1')
        h2 = self.addHost('h2')
        self.addLink(h1, s1)
        self.addLink(h2, s4)
        self.addLink(s1, s2)
        self.addLink(s1, s3)
        self.addLink(s2, s4)
        self.addLink(s3, s4)

class Multi2(Topo):
    def build(self):
        s1x1 = self.addSwitch('s1x1', dpid='101')
        s2x1 = self.addSwitch('s2x1', dpid='201')
        s2x2 = self.addSwitch('s2x2', dpid='202')
        s3x1 = self.addSwitch('s3x1', dpid='301')
        s3x2 = self.addSwitch('s3x2', dpid='302')
        s3x3 = self.addSwitch('s3x3', dpid='303')
        s4x1 = self.addSwitch('s4x1', dpid='401')
        s4x2 = self.addSwitch('s4x2', dpid='402')
        s5x1 = self.addSwitch('s5x1', dpid='501')
        h1 = self.addHost('h1')
        h2 = self.addHost('h2')
        self.addLink(h1, s1x1)
        self.addLink(h2, s5x1)
        self.addLink(s1x1, s2x1)
        self.addLink(s1x1, s2x2)
        self.addLink(s2x1, s3x1)
        self.addLink(s2x1, s3x2)
        self.addLink(s2x2, s3x2)
        self.addLink(s2x2, s3x3)
        self.addLink(s3x1, s4x1)
        self.addLink(s3x2, s4x1)
        self.addLink(s3x2, s4x2)
        self.addLink(s3x3, s4x2)
        self.addLink(s4x1, s5x1)
        self.addLink(s4x2, s5x1)

class Bottleneck(Topo):
    def build(self):
        s1x1 = self.addSwitch('s1x1', dpid='101')
        s2x1 = self.addSwitch('s2x1', dpid='201')
        s2x2 = self.addSwitch('s2x2', dpid='202')

        # Flag the 3rd layer of switches for routeman can tell they are limited
        s3x1 = self.addSwitch('s3x1', dpid='999301')
        s3x2 = self.addSwitch('s3x2', dpid='999302')
        s3x3 = self.addSwitch('s3x3', dpid='999303')

        s4x1 = self.addSwitch('s4x1', dpid='401')
        s4x2 = self.addSwitch('s4x2', dpid='402')
        s5x1 = self.addSwitch('s5x1', dpid='501')
        h1 = self.addHost('h1')
        h2 = self.addHost('h2')
        self.addLink(h1, s1x1)
        self.addLink(h2, s5x1)
        self.addLink(s1x1, s2x1)
        self.addLink(s1x1, s2x2)

        # All links to and from the 3rd layer of switches are limited
        self.addLink(s2x1, s3x1, bw=500, htb=True)
        self.addLink(s2x1, s3x2, bw=500, htb=True)
        self.addLink(s2x2, s3x2, bw=500, htb=True)
        self.addLink(s2x2, s3x3, bw=500, htb=True)
        self.addLink(s3x1, s4x1, bw=500, htb=True)
        self.addLink(s3x2, s4x1, bw=500, htb=True)
        self.addLink(s3x2, s4x2, bw=500, htb=True)
        self.addLink(s3x3, s4x2, bw=500, htb=True)

        self.addLink(s4x1, s5x1)
        self.addLink(s4x2, s5x1)

class Single(Topo):
    def build(self):
        s1 = self.addSwitch('s1')
        h1 = self.addHost('h1')
        h2 = self.addHost('h2')
        h3 = self.addHost('h3')
        self.addLink(h1, s1)
        self.addLink(h2, s1)
        self.addLink(h3, s1)

class Double(Topo):
    def build(self):
        s1 = self.addSwitch('s1')
        s2 = self.addSwitch('s2')
        h1 = self.addHost('h1')
        h2 = self.addHost('h2')
        h3 = self.addHost('h3')
        self.addLink(h1, s1)
        self.addLink(h2, s1)
        self.addLink(h3, s2)
        self.addLink(s1, s2)

# Tell mininet to print useful information
setLogLevel('info')
topos = {
    'single': (lambda: Single()),
    'double': (lambda: Double()),
    'topo1': (lambda: Topo1()),
    'multi1': (lambda: Multi1()),
    'multi2': (lambda: Multi2()),
    'bottleneck': (lambda: Bottleneck()),
    'torus3': (lambda: TorusTopo(x=3, y=3)),
    'torus4': (lambda: TorusTopo(x=4, y=4))
}

network = sys.argv[1]
print "Starting network: %s" % (network)
if network == 'bottleneck':
    net = Mininet(topo=topos[network](), controller=RemoteController, link=TCLink)
else:
    net = Mininet(topo=topos[network](), controller=RemoteController)
net.start()

# Add static network configuration
for host in net.hosts:
    host.setARP("10.10.10.10", "01:10:01:10:01:10")
    host.setDefaultRoute("dev %s via 10.10.10.10" % (host.intfs[0].name))

cli = CLI(net)

net.stop()
