#!/bin/bash
# Gary Friedman
# Copyright 2016

_usage() {
  echo "Usage: $0 <host> [mode] [name] [bandwidth]"
  echo -e "\t-h host\t\tMininet host to launch on (hostname or \"random\")"
  echo -e "\t-m mode\t\tMode to run in (freeflow or classic) [$MODE]"
  echo -e "\t-n name\t\tService name to use (if classic mode, port#) [$NAME]"
  echo -e "\t-b bandwidth\tBandwidth to register with [$BW]"
  echo -e "\t-x\t\tClose connections on each operation [false]"
  exit 1
}

_debug() {
  echo "=====> $*"
}

MODE=freeflow
NAME=default
BW=1
unset CONN_CLOSE
while getopts ":b:m:n:h:x" opt; do
  case $opt in
    b) BW=$OPTARG; _debug "Bandwidth: $BW" ;;
    h) HOST=$OPTARG; _debug "Host: $HOST" ;;
    m) MODE=$OPTARG; _debug "Mode: $MODE" ;;
    n) NAME=$OPTARG; _debug "Name: $NAME" ;;
    x) export CONN_CLOSE=true; _debug "Closing connections" ;;
    \?) _usage ;;
    :) echo "-$OPTARG requires an argument"; exit 1 ;;
  esac
done

[[ -z "$HOST" ]] && _usage

cd $(dirname $0)
exec ./exec.sh $HOST ./java/web-server/run.sh $MODE $NAME $BW
