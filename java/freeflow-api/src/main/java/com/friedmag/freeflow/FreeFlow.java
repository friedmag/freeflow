// Gary Friedman
// Copyright 2016
package com.friedmag.freeflow;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class FreeFlow {
	
	private static byte xid = 0;
	private final ScheduledExecutorService scheduler =
			Executors.newScheduledThreadPool(1);
	
	static public FreeFlow create() {
		return new FreeFlow();
	}
	
	static int unsign(short value) {
		return value >= 0 ? value : 0x10000 + value;
	}
	
	private DatagramSocket socket;
	
	private FreeFlow() {
		try {
			socket = new DatagramSocket();
			//System.out.println("Bound UDP socket: " + socket.getLocalPort());
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}
	
	public static class RegistrationInfo {
		public String name;
		public int port;
		public int mbPerSec;
		public Double id;
	}
	
	public void sendRegistration(RegistrationInfo info) {
		ByteBuffer buf = ByteBuffer.allocate(100);
		buf.putShort((short)0x4295); // magic
		buf.put((byte)0x01); // version
		buf.put((byte)0x01); // message
		buf.put(++xid); // transaction id
		buf.putShort((short)info.name.length());
		try {
			buf.put(info.name.getBytes("UTF8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		buf.putShort((short)info.port);
		buf.putInt(Math.max(1, info.mbPerSec));
		if (info.id != null) {
			buf.putDouble(info.id);
		}
		DatagramPacket packet = new DatagramPacket(buf.array(), buf.position());
		try {
			packet.setAddress(InetAddress.getByName("10.10.10.10"));
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		packet.setPort(41234);
		try {
			socket.send(packet);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public RegistrationInfo register(String name, int port, int mbPerSec) {
		RegistrationInfo ret = new RegistrationInfo();
		ret.name = name;
		ret.port = port;
		ret.mbPerSec = mbPerSec;
		sendRegistration(ret);
		try {
			//System.out.println("Registration sent! service=" + name + " port=" + port);
			// TODO: The next packet may not be exactly right... and how to handle errors?
			byte rxid;
			DatagramPacket packet = new DatagramPacket(new byte[100], 100);
//			do {
				interruptAfter(5000);
				socket.receive(packet);
				cancelInterrupt();
				ByteBuffer buf = ByteBuffer.wrap(packet.getData());
				if (buf.getShort() != 0x4295)
					System.out.println("Invalid magic header!");
				if (buf.get() != 0x01)
					System.out.println("Invalid version!");
				if (buf.get() != 0x02)
					System.out.println("Invalid response!");
				if ((rxid = buf.get()) != xid)
					System.out.println("Invalid xid!");
				ret.id = buf.getDouble();
				System.out.println("Registration response: " + rxid);
//			} while (rxid != xid);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ret;
	}
	
	public static class ConnectInfo {
		public InetAddress addr;
		public int port;
		public int localPort;
	}
	
	public static class QoSInfo {
		public int mbPerSec;
	}
	
	public ConnectInfo connect(String name, QoSInfo qos) {
		ConnectInfo ret = new ConnectInfo();
		ByteBuffer buf = ByteBuffer.allocate(100);
		buf.putShort((short)0x4295); // magic
		buf.put((byte)0x01); // version
		buf.put((byte)0x03); // message
		buf.put(++xid); // transaction id
		buf.putShort((short)name.length()); // service len
		try {
			buf.put(name.getBytes("UTF8")); // service
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		buf.putInt(qos.mbPerSec);
		DatagramPacket packet = new DatagramPacket(buf.array(), buf.position());
		try {
			packet.setAddress(InetAddress.getByName("10.10.10.10"));
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		packet.setPort(41234);
		try {
			socket.send(packet);
			System.out.println("Connect sent! service=" + name);
	
			// TODO: The next packet may not be exactly right... and how to handle errors?
			byte rxid;
//			do {
				packet.setData(new byte[256]);
				interruptAfter(5000);
				socket.receive(packet);
				cancelInterrupt();
				System.out.println("Received packet???");
				buf = ByteBuffer.wrap(packet.getData());
				if (buf.getShort() != 0x4295)
					System.out.println("Invalid magic header!");
				if (buf.get() != 0x01)
					System.out.println("Invalid version!");
				if (buf.get() != 0x04)
					System.out.println("Invalid response!");
				if ((rxid = buf.get()) != xid)
					System.out.println("Invalid xid!");
				byte[] dst = new byte[4];
				buf.get(dst);
				ret.addr = InetAddress.getByAddress(dst);
				ret.port = unsign(buf.getShort());
				System.out.println("Packet??? " + ret.addr.getHostAddress() + " and " + ret.port);
				if ("0.0.0.0".equals(ret.addr.getHostAddress())) {
					byte[] str = new byte[ret.port];
					buf.get(str);
					throw new RuntimeException("Connection error: " + new String(str, StandardCharsets.UTF_8));
				}
//				System.out.println("cap:" + buf.capacity() + " pos:" + buf.position());
//				for (int i = 0; i < 15; ++i)
//					System.out.println("BYTE " + i + ": " + Integer.toHexString(buf.get(i)));
				short tmp = buf.getShort();
				ret.localPort = unsign(tmp);
				System.out.println("Read host:" + ret.addr.getHostAddress() + " port:" + ret.port + " local:" + ret.localPort + " tmp:" + tmp + " rxid:" + rxid);
//			} while (rxid != xid);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ret;
	}
	
	ScheduledFuture<?> interrupter = null;
	
	private void interruptAfter(int ms) {
		final Thread self = Thread.currentThread();
		interrupter = scheduler.schedule(new Runnable() {
			public void run() {
				System.out.println("Interrupting thread!");
				self.interrupt();
			}
		}, ms, TimeUnit.MILLISECONDS);
	}
	
	private void cancelInterrupt() {
		interrupter.cancel(false);
	}
	
	public void shutdown() {
		scheduler.shutdown();
	}

}
