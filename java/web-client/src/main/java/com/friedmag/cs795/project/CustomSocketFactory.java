// Gary Friedman
// Copyright 2016
package com.friedmag.cs795.project;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

import org.apache.http.HttpHost;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.protocol.HttpContext;

import com.friedmag.freeflow.FreeFlow;
import com.friedmag.freeflow.FreeFlow.ConnectInfo;
import com.friedmag.freeflow.FreeFlow.QoSInfo;

public class CustomSocketFactory extends PlainConnectionSocketFactory {
	
	private FreeFlow freeFlow = FreeFlow.create();
	
	private static class MySocket extends Socket {
		@Override
		public synchronized void close() throws IOException {
			//System.out.println("RECEIVED CLOSE REQUEST!!!");
			super.close();
		}
	}

	@Override
	public Socket connectSocket(int connectTimeout, Socket socket, HttpHost host,
			InetSocketAddress remoteAddress, InetSocketAddress localAddress, HttpContext context)
			throws IOException {
		Socket ret;
		String service = (String) context.getAttribute("service");
		long start = System.currentTimeMillis(), end;
		if (service != null) {
			QoSInfo qos = new QoSInfo();
			qos.mbPerSec = Integer.parseInt((String) context.getAttribute("mbPerSec"));
			
			ConnectInfo info = freeFlow.connect(service, qos);
			end = System.currentTimeMillis();
			System.out.println("RECORD msg-connect," + (end - start));
			
			socket = new MySocket();
			InetSocketAddress newLocalAddress = new InetSocketAddress(info.localPort);
			InetSocketAddress newRemoteAddress = new InetSocketAddress(info.addr, info.port);
			ret = super.connectSocket(connectTimeout, socket, host, newRemoteAddress, newLocalAddress, context);
		} else {
			ret = super.connectSocket(connectTimeout, socket, host, remoteAddress, localAddress, context);
		}
		end = System.currentTimeMillis();
		System.out.println("RECORD " + end + ",connect," + (end - start));
		return ret;
	}

	public void shutdown() {
		freeFlow.shutdown();
	}

}
