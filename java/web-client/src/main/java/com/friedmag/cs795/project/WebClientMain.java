// Gary Friedman
// Copyright 2016
package com.friedmag.cs795.project;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

public class WebClientMain {
	
	static private final long WRITE_SIZE = 1 * 1024 * 1024 * 1024;

	public static void main(String[] args) throws Exception {
		if (args.length < 5) {
			System.out.println("Required args: host|freeflow port|service query(quick,read,write) repetitions bw");
			System.exit(1);
		}
		String mode = args[0];
		String connector = args[1];
		String query = args[2];
		int reps = Integer.parseInt(args[3]);
		String bw = args[4];
		String host = "localhost";
        HttpClientConnectionManager cm;
		HttpContext context = new BasicHttpContext();
		int port = 3232;
		CustomSocketFactory factory = new CustomSocketFactory();
		
		Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
				.register("http", factory)
				.build();
		cm = new BasicHttpClientConnectionManager(socketFactoryRegistry);
        if ("freeflow".equals(mode)) {
			context.setAttribute("service", connector);
			context.setAttribute("mbPerSec", bw);
        } else {
        	port = Integer.parseInt(connector);
			host = mode;
        }
        
        CloseableHttpClient httpClient = HttpClients.custom()
					.setConnectionManager(cm)
					.build();
        
        try {
        	for (int i = 0; i < reps; ++i) {
        		long start = System.currentTimeMillis();
        		int size = 0, read;
        		if ("quick".equals(query) || "sleep".equals(query)) {
        			String url = "/test/echo/hello";
        			if ("sleep".equals(query)) url = "/test/sleep/" + (reps * 1000L);
        			HttpGet httpGet = new HttpGet("http://" + host + ":" + port + url);
        			CloseableHttpResponse response1 = httpClient.execute(httpGet, context);
        			try {
        				//                System.out.println(response1.getStatusLine());
        				HttpEntity entity1 = response1.getEntity();
        				InputStream is = entity1.getContent();
        				StringBuilder str = new StringBuilder();
        				byte buf[] = new byte[1024];
        				while ((read = is.read(buf)) >= 0) {
        					str.append(new String(buf, 0, read, StandardCharsets.UTF_8));
        				}
        				size = str.length();
        				System.out.println("Read result: " + str);
        				// do something useful with the response body
        				// and ensure it is fully consumed
        				EntityUtils.consume(entity1);
        			} finally {
        				response1.close();
        			}
        		} else if ("read".equals(query)) {
        			HttpGet httpGet = new HttpGet("http://" + host + ":" + port + "/test/read");
        			CloseableHttpResponse response1 = httpClient.execute(httpGet, context);
        			try {
        				//                System.out.println(response1.getStatusLine());
        				HttpEntity entity1 = response1.getEntity();
        				InputStream is = entity1.getContent();
        				byte buf[] = new byte[1024];
        				while ((read = is.read(buf)) >= 0) size += read;
        				// do something useful with the response body
        				// and ensure it is fully consumed
        				EntityUtils.consume(entity1);
        			} finally {
        				response1.close();
        			}
        		} else if ("write".equals(query)) {
        			HttpPost httpPost = new HttpPost("http://" + host + ":" + port + "/test/write");
					FileInputStream fileInput = new FileInputStream(new File("data.dat"));
					InputStreamEntity entity = new InputStreamEntity(fileInput, -1, ContentType.APPLICATION_OCTET_STREAM);
        			httpPost.setEntity(entity);
        			CloseableHttpResponse response1 = httpClient.execute(httpPost, context);
        			try {
        				//        			System.out.println(response1.getStatusLine());
        				InputStream is = response1.getEntity().getContent();
        				StringBuilder str = new StringBuilder();
        				byte buf[] = new byte[1024];
        				while ((read = is.read(buf)) >= 0) {
        					str.append(new String(buf, 0, read, StandardCharsets.UTF_8));
        				}
        				System.out.println("Read result: " + str);
        				size = Integer.parseInt(str.toString());
        				// do something useful with the response body
        				// and ensure it is fully consumed
        				EntityUtils.consume(response1.getEntity());
        			} finally {
        				response1.close();
        			}
        		}
        		long end = System.currentTimeMillis();
        		long time = end - start;
        		System.out.println("RECORD " + end + "," + query + "," + size + "," + time);
       			if ("sleep".equals(query)) break; // only sleep once
        	}
        } finally {
        	factory.shutdown();
            httpClient.close();
        }
	}
}
