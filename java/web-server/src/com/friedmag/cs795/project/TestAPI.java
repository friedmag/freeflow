// Gary Friedman
// Copyright 2016
package com.friedmag.cs795.project;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.StreamingOutput;

@Path("/test")
public class TestAPI {
	
	static private final long WRITE_SIZE = 1 * 1024 * 1024 * 1024;
	static private boolean CONN_CLOSE = System.getenv("CONN_CLOSE") != null;
	
	private Response build(ResponseBuilder resp) {
		if (CONN_CLOSE) resp.header("Connection", "close");
		return resp.build();
	}

	@GET
	@Path("echo/{param}")
	public Response getMsg(@PathParam("param") String msg) {
		System.out.println("ECHO: msg=" + msg);
		return build(Response.ok("Connection Good: " + msg));
	}

	@GET
	@Path("sleep/{param}")
	public Response getMsg(@PathParam("param") long time) {
		System.out.println("SLEEP: time=" + time);
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return build(Response.ok("Sleep Done: " + time));
	}

	@GET
	@Path("read")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response read(@Context HttpServletRequest request) {
		System.out.println("Read: " + request.getRemoteAddr() + " : " + request.getRemotePort());
		final byte[] buf = new byte[1024 * 1024];
		StreamingOutput stream = new StreamingOutput() {
			private int written = 0;
			public void write(OutputStream os) throws IOException, WebApplicationException {
				while (written < WRITE_SIZE) {
					os.write(buf);
					written += buf.length;
				}
				os.flush();
				os.close();
			}
		};
		System.out.println("READ: called");
		return build(Response.ok(stream));
	}

	@POST
	@Path("write")
	@Consumes(MediaType.APPLICATION_OCTET_STREAM)
	public Response write(InputStream is) {
		byte[] buf = new byte[1024 * 1024];
		int total = 0, read;
		try {
			while ((read = is.read(buf)) > 0) total += read;
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("WRITE: Read " + total + " bytes");
		return build(Response.ok(total + ""));
	}

}
