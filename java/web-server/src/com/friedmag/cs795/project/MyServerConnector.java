// Gary Friedman
// Copyright 2016
package com.friedmag.cs795.project;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;

import com.friedmag.freeflow.FreeFlow;
import com.friedmag.freeflow.FreeFlow.RegistrationInfo;

public class MyServerConnector extends ServerConnector {
	
	private FreeFlow freeFlow;
	private String name;
	private int mbPerSec = 1;

	public MyServerConnector(Server server) {
		super(server);
		freeFlow = FreeFlow.create();
	}
	
	private final ScheduledExecutorService scheduler =
			Executors.newScheduledThreadPool(3);

	@Override
	public void open() throws IOException {
		super.open();
		
		long start = System.currentTimeMillis();
		final RegistrationInfo info = freeFlow.register(name, this.getLocalPort(), mbPerSec);
		long end = System.currentTimeMillis();
		System.out.println("RECORD msg-register," + (end - start));

		final Runnable registrar = new Runnable() {
			public void run() { freeFlow.sendRegistration(info); }
		};
		scheduler.scheduleAtFixedRate(registrar, 30, 30, TimeUnit.SECONDS);
	}
	
	// TODO: override close() if server ever stops

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMbPerSec() {
		return mbPerSec;
	}

	public void setMbPerSec(int mbPerSec) {
		this.mbPerSec = mbPerSec;
	}

	@Override
	public Future<Void> shutdown() {
		freeFlow.shutdown();
		return super.shutdown();
	}

}
