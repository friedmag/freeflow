// Gary Friedman
// Copyright 2016
'use strict';

const debug = require('debug')('freeflow:controller');
const of = require('node-of')();
const ofMac = "01:10:01:10:01:10";
const ofIP = "10.10.10.10";
const decoder = require('./decoder');

of.listen(6633);
of.magic_mac = ofMac;
of.magic_ip = ofIP;
var ServiceMan = require('./serviceman')(of);
var RouteMan = require('./routeman')(of);
require('../ui/server/main')(ServiceMan, RouteMan);

of.sendPacket = function(sw, dst_mac, dst_ip, dst_port, out_port, message, offset, confirm) {
  var data = this.gen_udp({
    dl_dst: dst_mac,
    dl_src: this.magic_mac,
    nw_dst: dst_ip,
    nw_src: this.magic_ip,
    dst: dst_port,
    src: 4242,
    data: offset ? message.slice(0, offset) : message,
  });

  var body = {
    data,
    actions: [{
      header: {
        type: 'OFPAT_OUTPUT',
      },
      body: {
        port: out_port,
      },
    }],
  };
  sw.send({
    header: {
      type: 'OFPT_PACKET_OUT',
    },
    body,
    confirm,
  });
};

of.on('OF_SWITCH', function(sw) {
  debug(`Switch! ${sw}`);

  var ports = {};

  // TODO: this should be processed in node-of, considering it sends the request
  // Pass things along to OF_SWITCH event
  sw.on('OFPT_FEATURES_REPLY', function(msg) {
    debug(`Received features from ${sw}: ${JSON.stringify(msg)}`);
    sw.name = `${parseInt(msg.body.datapath_id)}`;
    sw.ports = msg.body.ports;
    of.emit('switch_new', sw);

    sw.once('OF_SWITCH_LOST', () => {
      debug(`Lost switch`);
      sw.emit('lost');
    });

    // Add rule to direct registration packets to the controller
    sw.send({
      header: {
        type: 'OFPT_FLOW_MOD',
      },
      body: {
        command: 'OFPFC_ADD',
        priority: 0,
        match: {
          dl_dst: ofMac,
        },
        actions: [{
          header: {
            type: 'OFPAT_OUTPUT',
          },
          body: {
            port: 'OFPP_CONTROLLER',
            max_len: 9000,
          },
        }],
      }
    });
  });

  // TODO: deal with OFPT_PORT_STATUS
  // ie: {"reason":"OFPPR_MODIFY","desc":{"port_no":1,"hw_addr":"4e:4e:3d:8c:86:54","name":"s1-eth1","config":["OFPPC_PORT_DOWN"],"state":["OFPPS_LINK_DOWN"],"curr":["OFPPF_10GB_FD","OFPPF_COPPER"]}}

  sw.on('OFPT_FLOW_REMOVED', (ofmsg) => {
    sw.emit('flow-removed', ofmsg.body);
  });

  sw.on('OFPT_PACKET_IN', function(ofmsg) {
    debug(`Packet received from ${sw.name}`);
    var packet = of.packet(ofmsg.body.data);

    // Respond to UDP packets targeted at me!
    // TODO: install a flow for this directly that will send to me...
    if (packet.dl_dst === ofMac && packet.udp) {
      var msg = decoder.decode(ofmsg.body, packet);
      debug(`Received ${msg.type} from ${sw.name}`);
      sw.emit(msg.type, msg);
      return;
    } else {
      debug(`Packet received from ${sw.name}: ${JSON.stringify(ofmsg)}`);
    }
  });
});
