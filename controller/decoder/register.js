// Gary Friedman
// Copyright 2016
'use strict';

module.exports.decode = (data, offset) => {
  var serviceLen = data.readUInt16BE(offset); offset += 2;
  var service = data.toString('ascii', offset, offset + serviceLen); offset += serviceLen;
  var port = data.readUInt16BE(offset); offset += 2;
  var mbPerSec = data.readUInt32BE(offset); offset += 4;
  var token = 0;
  if (data.length >= (offset + 8)) {
    token = data.readDoubleBE(offset); offset += 8;
  }
  return {
    service,
    port,
    mbPerSec,
    token,
  };
};

