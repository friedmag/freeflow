// Gary Friedman
// Copyright 2016
'use strict';

module.exports.decode = (data, offset) => {
  var swLen = data.readUInt16BE(offset); offset += 2;
  var sw = data.toString('ascii', offset, offset + swLen); offset += swLen;
  return {
    sw,
  };
};


