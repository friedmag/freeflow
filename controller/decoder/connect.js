// Gary Friedman
// Copyright 2016
'use strict';

module.exports.decode = (data, offset) => {
  var serviceLen = data.readUInt16BE(offset); offset += 2;
  var service = data.toString('ascii', offset, offset + serviceLen); offset += serviceLen;
  var mbPerSec = data.readUInt32BE(offset); offset += 4;
  return {
    service,
    mbPerSec,
  };
};


