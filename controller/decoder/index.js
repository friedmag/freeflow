// Gary Friedman
// Copyright 2016
'use strict';

const decoders = {
  'register': require('./register'),
  'connect': require('./connect'),
  'discovery': require('./discovery'),
};

const messageTypes = {
  0: 'ping',
  1: 'register',
  2: 'register_complete',
  3: 'connect',
  4: 'connect_complete',
  0x80: 'discovery',
};

module.exports.decode = (body, packet) => {
  var offset = 0;
  var data = packet.udp.data;
  var magic = data.readUInt16BE(offset); offset += 2;
  if (magic !== 0x4295) {
    var msg = `Invalid magic header: ${magic}`;
    console.error(msg);
    throw msg;
  }
  var version = data.readUInt8(offset); offset += 1;
  if (version !== 1) {
    var msg = `Invalid version: ${version}`;
    console.error(msg);
    throw msg;
  }
  var messageType = data.readUInt8(offset); offset += 1;
  if (!(messageType in messageTypes)) {
    throw `Invalid message type ${messageType}!`;
  }
  var xid = data.readUInt8(offset); offset += 1;

  var messageName = messageTypes[messageType];
  var data = Object.assign(
        decoders[messageName].decode(data, offset),
        {
          xid,
          version,
          in_port: body.in_port,
          dl_src: packet.dl_src,
          nw_src: packet.ip.nw_src,
          tp_src: packet.udp.src,
          type: messageName,
        }
      );
  return data;
};
