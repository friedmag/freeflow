// Gary Friedman
// Copyright 2016
'use strict';

const debug = require('debug')('freeflow:routeman');
const EventEmitter = require('events');
const util = require('util');
const oflib = require('oflib-node');

const RouteMan = function(ct) {
  ct.on('switch_new', this.switchNew.bind(this));
  ct.on('route_add', this.routeAdd.bind(this));
  this.ct = ct;
  this.switches = {};
  this.flows = {};
  this.conns = {};
};

util.inherits(RouteMan, EventEmitter);

RouteMan.prototype.switchNew = function(sw) {
  debug(`RouteMan Switch! ${sw}`);

  // Initialize port information from feature list
  // TODO: get updates
  var portInfo = sw.portInfo = {};
  for (var port of sw.ports) {
    // Seems to only have state when link is down
    if (port.state.length == 0) {
      var obj = {used: {in: 0, out: 0}};
      // Hack so I can detect throttled ports for now
      if (sw.toString().startsWith("999")) {
        obj.speed = 500;
      } else if (port.curr[0] === "OFPPF_10GB_FD") {
        obj.speed = 10000;
      } else {
        // assume 1GB for now
        obj.speed = 1000;
      }
      portInfo[port.port_no] = obj;
    }
  }
  debug(`Read speeds: ${JSON.stringify(portInfo)}`);

  sw.ports = {};
  this.switches[sw.name] = sw;

  // Clear out any existing timer, and schedule a network scan in 1 second
  clearTimeout(this.timer);
  this.pending = Object.keys(this.switches);
  this.timer = setTimeout(this.switchScan.bind(this), 1000);

  sw.on('flow-removed', this.switchFlowRemoved.bind(this, sw));
  sw.on('discovery', this.switchDiscovery.bind(this, sw));
  sw.on('lost', this.switchLost.bind(this, sw));

  this.update();
  debug(`RouteMan ${sw}`);
};

RouteMan.prototype.switchScan = function(sw) {
  debug(`Scanning switches`);
  const ct = this.ct;
  for (var swname of this.pending) {
    var sw = this.switches[swname];
    debug(`Scan switch: ${sw.name}`);

    // Form packet
    var message = new Buffer(100);
    var offset = 0;
    message.writeUInt16BE(0x4295, offset); offset += 2;
    message.writeUInt8(1, offset); offset += 1;
    message.writeUInt8(0x80, offset); offset += 1;
    message.writeUInt8(0, offset); offset += 1; // filler xid
    message.writeUInt16BE(sw.name.length, offset); offset += 2;
    message.write(sw.name, offset); offset += sw.name.length;

    this.ct.sendPacket(sw, ct.magic_mac, ct.magic_ip, 4242, 'OFPP_ALL', message, offset);
  }
  this.pending = [];
};

RouteMan.prototype.dijkstra = function(src, dst, cost) {
  var Q = [];
  var dist = {};
  var prev = {};
  var port = {};

  for (var name in this.switches) {
    dist[name] = Infinity;
    Q.push(name);
  }

  dist[src] = 0;

  while (Q.length > 0) {
    var i = 0;
    for (var j in Q)
      if (dist[Q[j]] < dist[Q[i]]) {
        i = j;
      }
    var u = Q[i];

    if (u === dst) {
      break;
    }

    Q.splice(i, 1);

    var sw = this.switches[u];
    var ports = sw.ports;
    for (var j in ports) {
      var v = ports[j];
      var out = sw.portInfo[j].used.out;
      var alt = dist[u] + out;
      if ((out + cost) > sw.portInfo[j].speed) alt = Infinity;
      if (alt < dist[v]) {
        dist[v] = alt;
        prev[v] = u;
        port[v] = {sw: u, port: j};
      }
    }
  }

  var path = [];
  debug(`Dst: ${dst} Cost: ${dist[dst]}`);
  while (prev[dst]) {
    path.push(port[dst]);
    dst = prev[dst];
  }

  debug(`Path: ${JSON.stringify(path)}`);
  debug(`Prev: ${JSON.stringify(prev)}`);
  debug(`Dist: ${JSON.stringify(dist)}`);
  debug(`Port: ${JSON.stringify(port)}`);
  return path;
};

RouteMan.prototype.switchLost = function(sw) {
  debug(`Switch lost: ${JSON.stringify(sw)}`);
  // for now - just nuke it
  // NOTE: must be done first so new connection routes are valid!
  delete this.switches[sw.name];
  for (var swname in this.switches) {
    var nsw = this.switches[swname];
    for (var port in nsw.ports) {
      debug(`delete? ${port} ${sw.name} -> ${nsw.ports[port]}`);
      if (nsw.ports[port] === sw.name) {
        debug(`delete! ${port}`);
        delete nsw.ports[port];
      }
    }
  }
  // Find related connections
  for (var key in this.conns) {
    var c = this.conns[key];
    if (c.srcPath.findIndex((d) => d.sw === sw.name) >= 0) {
      debug(`Connection ${key} SRC needs re-routing!`);
      for (var flowKey of c.flows) this.removeConnection(flowKey);
      this.createConnection(c.use_ip, c.use_port, c.recv_port, c.src, c.dst);
    }
    if (c.dstPath.findIndex((d) => d.sw === sw.name) >= 0) {
      debug(`Connection ${key} DST needs re-routing!`);
    }
  }

  this.update();
};

RouteMan.prototype.removeConnection = function(key) {
  var flow = this.flows[key];
  if (!flow) return;
  debug(`Removing key ${key}: ${JSON.stringify(flow)}`);
  for (var step of flow.path) {
    if (!this.switches[step.sw]) continue;
    this.switches[step.sw].portInfo[step.port].used[step.dir] -= flow.bw;
    debug(`GBF Remove for ${key} on ${step.sw} port ${step.port} dir ${step.dir} - ${flow.bw} = ${this.switches[step.sw].portInfo[step.port].used[step.dir]}`);
  }
  delete this.conns[flow.key];
  delete this.flows[key];
};

RouteMan.prototype.switchFlowRemoved = function(sw, msg) {
  var key = msg.cookie.readDoubleBE(0, true);
  this.removeConnection(key);
  this.update();
};

RouteMan.prototype.switchDiscovery = function(sw, msg) {
  debug(`Switch discovery: ${JSON.stringify(sw)} -> ${JSON.stringify(msg)}`);
  this.switches[sw.name].ports[msg.in_port] = msg.sw;
  this.update();
};

var flowModForReceiver = (tgt, opts, cookie) => {
  return {
    header: {
      type: 'OFPT_FLOW_MOD',
    },
    body: {
      command: 'OFPFC_ADD',
      priority: 50, // higher priority than ForSender since a more specific match
      idle_timeout: 30,
      match: {
        dl_type: oflib.packet.ETH_TYPE_IP,
        nw_proto: 6, // TODO: protos this should be a constant someplace...
        // src/dst being the same means this packet is targeted for me
        nw_src: opts.use_ip,
        tp_src: opts.use_port,
        nw_dst: opts.use_ip,
        tp_dst: opts.use_port,
      },
      cookie,
      flags: ['OFPFF_SEND_FLOW_REM'],
      actions: [{
        header: {
          type: 'OFPAT_SET_DL_DST',
        },
        body: {
          dl_addr: tgt.dl_src,
        },
      }, {
        header: {
          type: 'OFPAT_SET_NW_DST',
        },
        body: {
          nw_addr: tgt.nw_src,
        },
      }, {
        header: {
          type: 'OFPAT_SET_TP_DST',
        },
        body: {
          tp_port: tgt.port || opts.flag,
        },
      }, {
        header: {
          type: 'OFPAT_OUTPUT',
        },
        body: {
          port: tgt.in_port,
        },
      }],
    },
    confirm: true,
  };
};

var flowModForSender = (tgt, opts, out_port, cookie) => {
  var ret = {
    header: {
      type: 'OFPT_FLOW_MOD',
    },
    body: {
      command: 'OFPFC_ADD',
      priority: 42,
      idle_timeout: 30,
      match: {
        dl_type: oflib.packet.ETH_TYPE_IP,
        nw_proto: 6, // TODO: protos this should be a constant someplace...
        nw_dst: opts.use_ip,
        tp_dst: opts.use_port,
        in_port: tgt.in_port,
      },
      cookie,
      flags: ['OFPFF_SEND_FLOW_REM'],
      actions: [{
        header: {
          type: 'OFPAT_SET_NW_SRC',
        },
        body: {
          nw_addr: opts.use_ip,
        },
      }, {
        header: {
          type: 'OFPAT_SET_TP_SRC',
        },
        body: {
          tp_port: opts.use_port,
        },
      }, {
        header: {
          type: 'OFPAT_OUTPUT',
        },
        body: {
          port: out_port,
        },
      }],
    },
    confirm: true,
  };

  if (opts.flag) {
    ret.body.actions.unshift({
      // Used to indicate return path
      // TODO: use in_port in match instead!
      header: {
        type: 'OFPAT_SET_VLAN_PCP',
      },
      body: {
        vlan_pcp: 1,
      },
    });
  }

  return ret;
};

var flowModForForwarder = (opts, out_port, cookie) => {
  var ret = {
    header: {
      type: 'OFPT_FLOW_MOD',
    },
    body: {
      command: 'OFPFC_ADD',
      priority: 42,
      idle_timeout: 30,
      match: {
        dl_type: oflib.packet.ETH_TYPE_IP,
        nw_proto: 6, // TODO: protos this should be a constant someplace...
        nw_dst: opts.use_ip,
        tp_dst: opts.use_port,
      },
      cookie,
      actions: [{
        header: {
          type: 'OFPAT_OUTPUT',
        },
        body: {
          port: out_port,
        },
      }],
    },
    confirm: true,
  };

  // Return flows need to match on vlan = 1, and have a higher priority
  if (opts.flag) {
    ret.body.priority = 50;
    ret.body.match.dl_vlan_pcp = 1;
  }

  return ret;
};

RouteMan.prototype.findPath = function(src, dst) {
  // TODO: need bidirectional paths to improve flow (so I can properly define in_port) - need to be able to tell s1p1 goes to s2p1 or whatever
  return this.dijkstra(src.sw.name, dst.sw.name, src.mbPerSec);
}

RouteMan.prototype.createFlow = function(path, src, dst, opts, flowKeys, savePath) {
  // TODO: need bidirectional paths to improve flow (so I can properly define in_port) - need to be able to tell s1p1 goes to s2p1 or whatever
  if (path.length === 0) throw new Error("No possible route found");

  var cookie = new Buffer(8);
  cookie.writeUInt32BE(opts.use_ip, 0, true);
  cookie.writeUInt16BE(opts.use_port, 4, true);
  cookie.writeUInt16BE(opts.flag ? 1 : 0, 6, true);
  debug(`GBF LOOK AT BUF: ${cookie.hexSlice(0,8)}`);
  var key = cookie.readDoubleBE(0, true);
  var flow = this.flows[key] = {key: opts.key, bw: opts.bw, path: []};
  flowKeys.push(key);

  var pathHelper = (flow, popts) => {
    this.switches[popts.sw].portInfo[popts.port].used[popts.dir]  += flow.bw;
    debug(`GBF Add for ${key} on ${popts.sw} port ${popts.port} dir ${popts.dir} + ${flow.bw} = ${this.switches[popts.sw].portInfo[popts.port].used[popts.dir]}`);
    flow.path.push(popts);
    savePath.push(popts);
  };

  debug(`Using cookie key: ${key}`);
  var step = path.pop();
  var promises = [];
  promises.push(this.switches[src.sw.name].send(
        flowModForSender(src, opts, step.port, cookie)));
  // TODO: actually set bw limit???
  pathHelper(flow, {sw: src.sw.name, port: src.in_port, dir: 'in'});
  pathHelper(flow, {sw: src.sw.name, port: step.port,   dir: 'out'});

  for (var step of path) {
    promises.push(this.switches[step.sw].send(
          flowModForForwarder(opts, step.port, cookie)));
    pathHelper(flow, {sw: step.sw, port: step.port, dir: 'out'});
  }

  promises.push(this.switches[dst.sw.name].send(
        flowModForReceiver(dst, opts, cookie)));
  pathHelper(flow, {sw: dst.sw.name, port: dst.in_port, dir: 'out'});
  return Promise.all(promises);
};

RouteMan.prototype.createConnection = function(use_ip, use_port, recv_port, src, dst) {
  return new Promise((resolve, reject) => {
    if (typeof(dst) === 'string') throw new Error(`Unable to find service: ${dst}`);
    var key = use_ip + "-" + use_port;
    debug(`GBF Creating connection for ${key}`);

    // Check if endpoint links have sufficient capacity
    var checkEndpoint = (sw, port, bw, dir) => {
      var portInfo = this.switches[sw].portInfo[port];
      if ((portInfo.used[dir] + bw) > portInfo.speed)
        throw new Error(`Insufficient bandwidth sw:${sw} port:${port} dir:${dir}`);
    };
    checkEndpoint(src.sw.name, src.in_port, src.mbPerSec, 'in');
    checkEndpoint(dst.sw.name, dst.in_port, dst.mbPerSec, 'out');

    // Connection info used for failover
    var conn = this.conns[key] = {
      key, use_ip, use_port, recv_port, src, dst,
      flows: [],
      srcPath: [],
      dstPath: [],
    };
    // Flow info used for tracking
    var opts = {
      key, use_ip, use_port,
      bw: src.mbPerSec,
      flag: false,
    };
    // Find both paths up front, so if there is none we error out before any modifications
    var srcPath = this.findPath(src, dst);
    var dstPath = this.findPath(dst, src);
    var promises = [];
    promises.push(this.createFlow(
          srcPath, src, dst, opts, conn.flows, conn.srcPath));
    Object.assign(opts, {
      bw: dst.mbPerSec,
      flag: recv_port,
    });
    promises.push(this.createFlow(
          dstPath, dst, src, opts, conn.flows, conn.dstPath));
    Promise.all(promises).then(resolve);
  });
};

RouteMan.prototype.routeAdd = function(src, dst) {
  debug(`RouteMan add route (src): ${JSON.stringify(src)}`);
  debug(`RouteMan add route (dst): ${JSON.stringify(dst)}`);

  // Generate random 48-bit key
  var use_port = (Math.random() * Math.pow(2, 16)) | 0;
  var recv_port = (Math.random() * Math.pow(2, 16)) | 0; // used for client-side receive port
  var use_ip, top_octet;
  do {
    use_ip = (Math.random() * Math.pow(2, 32)) | 0;
    // Filter out 0.* 10.*, 127.* and 224-239.*
    // Invalid, internal, localhost, and multicast respectively
    top_octet = (use_ip & 0xff000000) >>> 24;
  } while (top_octet === 0 || top_octet === 10 || top_octet === 127 || (top_octet >= 224 && top_octet <= 239));

  var message = new Buffer(100);
  var offset = 0;
  message.writeUInt16BE(0x4295, offset); offset += 2;
  message.writeUInt8(1, offset); offset += 1;
  message.writeUInt8(4, offset); offset += 1;
  message.writeUInt8(src.xid, offset); offset += 1; // respond with same xid

  debug(`Creating connection...`);
  this.createConnection(use_ip, use_port, recv_port, src, dst)
    .then(() => {
      debug(`Received response!`);
      message.writeUInt32BE(use_ip, offset, true); offset += 4;
      message.writeUInt16BE(use_port, offset, true); offset += 2;
      message.writeUInt16BE(recv_port, offset, true); offset += 2;
      debug(`SENDING BACK TO: ${JSON.stringify(src)} use_ip:${use_ip} use_port:${use_port} recv_port:${recv_port}`);
    })
    .catch((e) => {
      debug(`Received error!`);
      debug(`Received error: ${e.stack}`);
      // Send response error
      message.writeUInt32BE(0, offset, true); offset += 4;
      message.writeUInt16BE(e.message.length, offset, true); offset += 2;
      message.write(e.message, offset); offset += e.message.length;
      debug(`SENDING BACK TO: ${JSON.stringify(src)} error:${e.message}`);
    })
    .then(() => {
      debug(`Followup!`);
      this.ct.sendPacket(
          src.sw, src.dl_src, src.nw_src, src.tp_src, src.in_port,
          message, offset);
    });

  this.update();
};

RouteMan.prototype.network = function() {
  // Provides detailed network information for use in the UI for debug
  var ret = {nodes: [], links: []};
  var tmp = {};
  var i = 0;
  for (var name in this.switches) {
    var sw = this.switches[name];
    ret.nodes.push({name: sw.name});
    tmp[sw.name] = i++;
  }
  for (var name in tmp) {
    var sw = this.switches[name];
    for (var port in sw.ports) {
      ret.links.push({
        source: tmp[name],
        target: tmp[sw.ports[port]],
        used: sw.portInfo[port].used.out,
        total: sw.portInfo[port].speed,
        port
      });
    }
  }
  ret.conns = this.conns;
  return ret;
};

RouteMan.prototype.update = function() {
  this.emit('update', this.network());
};

module.exports = (ct) => new RouteMan(ct);
