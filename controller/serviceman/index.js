// Gary Friedman
// Copyright 2016
'use strict';

const debug = require('debug')('freeflow:serviceman');

const ServiceMan = function(ct) {
  this.ct = ct;
  this.cache_name = {};
  this.cache_id = {};
  this.timers = {};

  // When a new switch is discovered, listen for application messages from it
  ct.on('switch_new', (sw) => {
    debug(`Service Switch! ${sw}`);

    sw.on('register', this.register.bind(this, sw));

    sw.on('connect', this.connect.bind(this, sw));
  });
};

ServiceMan.prototype.invalidateRegistration = function(msg) {
  debug(`Invalidating registration: ${msg.service}`);
  delete this.cache_id[msg.id];
  delete this.cache_name[msg.service];
};

ServiceMan.prototype.resetRegistration = function(msg) {
  // Reset the given registration's timeout (it has been re-registered)
  clearTimeout(this.timers[msg.id]);
  // TODO: timeout should be configurable, not hard-coded 35 seconds
  this.timers[msg.id] = setTimeout(this.invalidateRegistration.bind(this, msg), 35000);
  // TODO: something more graceful. Adding this because if two services register with different id's but the same name, and one goes away, the cache becomes invalid.
  this.cache_name[msg.service] = [msg];
};

ServiceMan.prototype.addRegistration = function(msg) {
  // use token, if provided
  if (msg.token) msg.id = msg.token;
  // generate a valid token - ie, not in use
  else do { msg.id = Math.random(); } while (this.cache_id[msg.id]);
  debug(`Adding registration: ${JSON.stringify(msg)}`);
  this.cache_id[msg.id] = msg;
  // TODO: allow multiple registrations, choose from valid ones
  this.cache_name[msg.service] = [msg]; //(this.cache_name[msg.service] || []).concat(msg);
}

ServiceMan.prototype.register = function(sw, msg) {
  msg.sw = sw;
  debug(`Service register msg: ${JSON.stringify(msg)}`);
  if (msg.token) {
    var existing = this.cache_id[msg.token];
    if (existing) {
      debug(`Existing registration: ${existing.service}`);
      this.resetRegistration(existing);
    } else {
      debug(`Received unknown service: ${msg.service}`);
      this.addRegistration(msg);
    }
  } else {
    this.addRegistration(msg);
    debug(`Services: ${JSON.stringify(this.cache_name)}`);
    debug(`Services: ${JSON.stringify(this.cache_id)}`);

    var message = new Buffer(100);
    var offset = 0;
    message.writeUInt16BE(0x4295, offset); offset += 2;
    message.writeUInt8(1, offset); offset += 1;
    message.writeUInt8(2, offset); offset += 1;
    message.writeUInt8(msg.xid, offset); offset += 1; // respond with same xid
    message.writeDoubleBE(msg.id, offset); offset += 8;
    this.ct.sendPacket(sw, msg.dl_src, msg.nw_src, msg.tp_src, msg.in_port, message, offset);
    this.resetRegistration(msg);
  }

  // TODO: deregistration event!
};

ServiceMan.prototype.connect = function(sw, msg) {
  msg.sw = sw;
  debug(`Service connect msg: ${JSON.stringify(msg)}`);
  var services = this.cache_name[msg.service];
  if (services && services.length > 0) {
    debug(`Want to connect to: ${JSON.stringify(services)}`);
    // Emit event for the route manager to pick up
    this.ct.emit('route_add', msg, services[0]);
  } else {
    debug(`Unknown service: ${msg.service}`);
    // Emit event for the route manager to pick up... though it
    // just errors out in this case.
    this.ct.emit('route_add', msg, msg.service);
  }
};

ServiceMan.prototype.services = function() {
  // Publish a list of services. Could be useful to show on UI
  // for debug.  Could also expose it to applications with a new
  // message, but that would be more complex/needs to be able to
  // apply filters for authorization.
  var ret = [];
  for (var id in this.cache_id) {
    var service = this.cache_id[id];
    ret.push({
      service: service.service,
      mbPerSec: service.mbPerSec,
      sw: service.sw.name,
    });
  }
  return {services: ret};
};

module.exports = (ct) => new ServiceMan(ct);
