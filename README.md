# Dependencies

* Maven

# Install/Build

```
npm install
pushd java/freeflow-api
mvn package install
popd
pushd java/web-server
mvn package
popd
pushd java/web-client
mvn package
popd
rm -rf node_modules/oflib-node
git clone https://github.com/friedmag/oflib-node.git node_modules/oflib-node
```

# Running

## Controller

The UI will be available at http://<your IP>:3000/

```
./run-controller.sh
```

## Mininet

Available networks: torus4, torus3, single, double, topo1, multi1, multi2, and bottleneck.

```
./run-mn.sh torus4
```

## Web Server

```
./run-server.sh -?
./run-server.sh -h h1x1
```

## Web Client

```
./run-client.sh -?
./run-client.sh -h h4x4
```

## Ryu (STP)

```
./run-ryu.sh
```
